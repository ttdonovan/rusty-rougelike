# Rusty Rougelike

Inspired from [Rougelike Tutorial - In Rust](http://bfnightly.bracketproductions.com/rustbook/chapter_0.html).

### Development

#### **Clippy** - Rust "linter"

```
rustup component add clippy
cargo clippy
```