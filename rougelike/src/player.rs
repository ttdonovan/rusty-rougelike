use rltk::{VirtualKeyCode, Rltk, Point, console};
use specs::prelude::*;

use super::{Position, Player, State, Map, Viewshed, RunState, CombatStats, WantsToMelee};

use std::cmp::{min, max};

pub fn try_move_player(delta_x: i32, delta_y: i32, ecs: &mut World) {
    let mut positions = ecs.write_storage::<Position>();
    let mut players = ecs.write_storage::<Player>();
    let mut viewsheds = ecs.write_storage::<Viewshed>();
    let entities = ecs.entities();
    let combat_stats = ecs.read_storage::<CombatStats>();
    let map = ecs.fetch::<Map>();

    for (ent, _player, pos, viewshed) in (&entities, &mut players, &mut positions, &mut viewsheds).join() {
        if pos.x + delta_x < 1 || pos.x + delta_x > map.width-1 || pos.y + delta_y < 1 || pos.y + delta_y > map.height-1 { return; }
        let destination_idx = map.xy_idx(pos.x + delta_x, pos.y + delta_y);

        for potential_target in map.tile_content[destination_idx].iter() {
            let target = combat_stats.get(*potential_target);
            if let Some(_t) = target {
                console::log("From Hell's Heart, I stab thee!");
                let mut wants_to_melee = ecs.write_storage::<WantsToMelee>();
                wants_to_melee.insert(ent, WantsToMelee { target: *potential_target }).expect("Add target failed");
                return;
            }
        }

        if !map.blocked[destination_idx] {
            pos.x = min(79, max(0, pos.x + delta_x));
            pos.y = min(49, max(0, pos.y + delta_y));

            let mut ppos = ecs.write_resource::<Point>();
            ppos.x = pos.x;
            ppos.y = pos.y;

            viewshed.dirty = true;
        }
    }
}

pub fn player_input(gs: &mut State, ctx: &mut Rltk) -> RunState {
    // Player movement
    match ctx.key {
        None => { RunState::AwaitingInput }, // Nothing happened
        Some(key) => {
            use VirtualKeyCode::{
                A, D, W, S,
                Left, Right, Up, Down,
                H, L, K, J,
                Numpad4, Numpad6, Numpad8, Numpad2,
                Y, U, N, B,
                Numpad9, Numpad7, Numpad3, Numpad1,
            };

            let direction = match key {
                // Cardinal
                A | Left | H | Numpad4 => Some((-1, 0)),
                D | Right | L | Numpad6 => Some((1, 0)),
                W | Up | K | Numpad8 => Some((0, -1)),
                S | Down | J | Numpad2 => Some((0, 1)),

                // Diagonals
                Y | Numpad9 => Some((1, -1)),
                U | Numpad7 => Some((-1, -1)),
                N | Numpad3 => Some((1, 1)),
                B | Numpad1 => Some((-1, 1)),

                _ => None,
            };

            if let Some(d) = direction {
                try_move_player(d.0, d.1, &mut gs.ecs);
                RunState::PlayerTurn
            } else {
                RunState::AwaitingInput
            }
        }
    }
}
