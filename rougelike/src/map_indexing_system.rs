use specs::prelude::*;
use super::{Map, Position, BlocksTile};

pub struct MapIndexingSystem {}

impl<'a> System<'a> for MapIndexingSystem {
    type SystemData = (
        WriteExpect<'a, Map>,
        ReadStorage<'a, Position>,
        ReadStorage<'a, BlocksTile>,
        Entities<'a>,
    );

    fn run(&mut self, data: Self::SystemData) {
        let (mut map, positions, blockers, entities) = data;

        map.populate_blocked();
        map.clear_content_index();

        for (ent, pos) in (&entities, &positions).join() {
            let idx = map.xy_idx(pos.x, pos.y);

            // If they block, update the blocking list
            let b : Option<&BlocksTile> = blockers.get(ent);
            if let Some(_b) = b {
                map.blocked[idx] = true;
            }

            // Push the entity to the approprate index slot.
            // It's a Copy type, so we don't need to clone it (we want to avoid moving it out of the ECS!)
            map.tile_content[idx].push(ent);
        }
    }
}